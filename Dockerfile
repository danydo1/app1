# 1. Use an official Node.js runtime as the base image
FROM node:16-alpine

# 2. Set the working directory in the container
WORKDIR /usr/src/app

# 3. Copy package.json and package-lock.json for dependency installation
COPY package*.json ./

# 4. Install dependencies
RUN npm install

# 5. Copy the application source code to the working directory
COPY . .

# 6. Expose the application port
EXPOSE 3000

# 7. Define the command to run the application
CMD ["npm", "start"]
